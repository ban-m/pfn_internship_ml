﻿# Machine learning Assignment for PFN summer internship

## Infos

- Author: Bansho Masutani 
- Email: ban-m@g.ecc.u-tokyo.ac.jp
- Date: 2019/05/04
- Language: Rust
- Requirements: Rust compiler, `cargo`

## How to build

In summary, ./src/gnn.rs defines the graph neural network and its 'readout', whereas ./src/lib.rs define the classifier used in this assignment.

### Assignemnt 1

The source code locates at ./src/lib.rs. To run test, just type`cargo --release test`.

### Assignemnt 2

The source code locates at ./src/bin/assignment2.rs. To run the program and write the stdout to [File], type `cargo run --release --bin assignment2 > [File]`

### Assignment 3

The source code is ./src/bin/assignment3.rs. To run the program with its input as [Input Path], type `cargo run --release --bin assignment3 -- [Input Path]` 

### Assignment 4

The source code are ./src/bin/assignment4.rs and ./src/bin/assignment4-2.rs. To run the program with its input as [Train Data] [Test Data] and output as [Output], type
`cargo run --release --bin assignment4-2 -- [Train Data] [Test Data] > [Output]`
