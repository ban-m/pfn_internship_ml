extern crate machine_learning;
extern crate rand;
use machine_learning::gnn;
use rand::{thread_rng, Rng};

// generate a adjacency matrix for a graph has `size` nodes.
// With probability p, a edge between two nodes would be created.
// note that this adj-matrix is symmetric and always m[i][i] = false.
fn gen_graph(p: f64, size: usize) -> Option<gnn::GraphNeuralNetwork> {
    let mut rng = thread_rng();
    let mut edges = vec![vec![false; size]; size];
    for i in 0..size {
        for j in 0..size {
            if j > i {
                edges[i][j] = rng.gen_bool(p);
            } else if i > j {
                edges[i][j] = edges[j][i];
            }
        }
    }
    gnn::GraphNeuralNetwork::new(&edges)
}

fn main() {
    println!("id\titeration\tanswer\tpredict\tloss");
    for i in 0..10 {
        if test(true,i).is_none() {
            return;
        };
        if test(false,i).is_none() {
            return;
        };
    }
}


fn test(answer: bool,id:usize) -> Option<()> {
    let gnn = gen_graph(0.7,10)?;
    let mut learner = machine_learning::GNNClassifier::new_default();
    for i in 0..10000 {
        learner = learner.naive_update(&gnn, answer).unwrap();
        if i % 100 == 0{
            let loss = learner.loss(&gnn, answer).unwrap();
            let predict = learner.predict(&gnn).unwrap();
            let answer = if answer { 1 } else { 0};
            let predict = if predict { 1 } else { 0 };
            println!("{}\t{}\t{}\t{}\t{}",id,i,answer, predict,loss);
        }
    }
    Some(())
}
