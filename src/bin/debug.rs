extern crate machine_learning;
extern crate rand;
use machine_learning::gnn::GraphNeuralNetwork;
use machine_learning::GNNClassifier;
use rand::{thread_rng, Rng};
// The loss and accuracy of trainning dataset and the those of test dataset.
type Summary = (f64, f64, f64, f64);
type DataSet = Vec<(GraphNeuralNetwork, bool)>;
const EPOCH: usize = 150;
const BATCH: usize = 100;

// generate a adjacency matrix for a graph has `size` nodes.
// With probability p, a edge between two nodes would be created.
// note that this adj-matrix is symmetric and always m[i][i] = false.
fn gen_graph(p: f64, size: usize, answer: bool) -> DataSet {
    let mut rng = thread_rng();
    let mut edges = vec![vec![false; size]; size];
    for i in 0..size {
        for j in 0..size {
            if j > i {
                edges[i][j] = rng.gen_bool(p);
            } else if i > j {
                edges[i][j] = edges[j][i];
            }
        }
    }
    (0..1000)
        .filter_map(|_| Some((GraphNeuralNetwork::new(&edges)?, answer)))
        .collect()
}

fn main() {
    let dataset = gen_graph(0.7, 10, true);
    let (train, test) = split_input(dataset);
    let naive = naive_sgd(&train, &test, EPOCH, BATCH).unwrap();
    let momentum = momentum_sgd(&train, &test, EPOCH, BATCH).unwrap();
    println!("Method\tType\tEpoch\tLoss\tAccuracy");
    for (epoch, result) in naive.into_iter().enumerate() {
        println!("Naive\tTrain\t{}\t{}\t{}", epoch, result.0, result.1);
        println!("Naive\tTest\t{}\t{}\t{}", epoch, result.2, result.3);
    }
    for (epoch, result) in momentum.into_iter().enumerate() {
        println!("Momentum\tTrain\t{}\t{}\t{}", epoch, result.0, result.1);
        println!("Momentum\tTest\t{}\t{}\t{}", epoch, result.2, result.3);
    }
}

fn split_input(input: DataSet) -> (DataSet, DataSet) {
    let mut rng = thread_rng();
    input.into_iter().partition(|_| rng.gen_bool(0.99))
}

// Carry out naive SGD learning on the given dataset.
fn naive_sgd(train: &DataSet, test: &DataSet, epoch: usize, batch: usize) -> Option<Vec<Summary>> {
    GNNClassifier::new_default()
        .stochastic_gradient_descent(train, epoch, batch, test)
        .map(|e| e.1)
}


// Carry out momentum SGD learning on the given dataset.
fn momentum_sgd(
    train: &DataSet,
    test: &DataSet,
    epoch: usize,
    batch: usize,
) -> Option<Vec<Summary>> {
    GNNClassifier::new_default()
        .momentum_sgd(train, epoch, batch, test)
        .map(|e| e.1)
}
