extern crate machine_learning;
extern crate rand;
use machine_learning::gnn::GraphNeuralNetwork;
use machine_learning::GNNClassifier;
const EPOCH: usize = 200;
const BATCH: usize = 10;

fn main() -> std::io::Result<()> {
    let args: Vec<_> = std::env::args().collect();
    let dataset = machine_learning::io::open_input(&args[1])?;
    let tests = open_test(&args[2])?;
    let (learner,result) = GNNClassifier::new_default()
        .momentum_sgd(&dataset, EPOCH, BATCH, &vec![]).unwrap();
    for (epoch, result) in result.into_iter().enumerate() {
        eprintln!("Momentum\tTrain\t{}\t{}\t{}", epoch, result.0, result.1);
    }
    for test in tests{
        let predict = learner.predict(&test).unwrap();
        println!("{}",if predict { 1 } else { 0 });
    }
    Ok(())
}

fn open_test(root:&str)->std::io::Result<Vec<GraphNeuralNetwork>>{
    let data_num = 500;
    let mut data = vec![];
    for num in 0..data_num {
        let graph = format!("{}/{}_graph.txt", root, num);
        data.push(open_graph(&graph)?);
    }
    Ok(data)
}

fn open_graph(path: &str) -> std::io::Result<GraphNeuralNetwork> {
    use std::io::Read;
    let mut file = std::fs::File::open(std::path::Path::new(path))?;
    let mut input = String::new();
    file.read_to_string(&mut input)?;
    let mut nodes = 0;
    let mut graph:Vec<Vec<_>> = vec![];
    for (index,line) in input.lines().enumerate(){
        if index == 0{
            nodes = line.parse().unwrap();
        }else{
            graph.push(line.split_whitespace().map(|field| field == "1").collect())
        }
    }
    assert!(graph.len() == nodes && graph.iter().all(|e|e.len() == nodes));
    Ok(GraphNeuralNetwork::new(&graph).unwrap())
}


