extern crate machine_learning;
extern crate rand;
use machine_learning::gnn::GraphNeuralNetwork;
use machine_learning::GNNClassifier;
use rand::{thread_rng, Rng};
// The loss and accuracy of trainning dataset and the those of test dataset.
type Summary = (f64, f64, f64, f64);
type DataSet = Vec<(GraphNeuralNetwork, bool)>;
const EPOCH: usize = 150;
const BATCH: usize = 10;

fn main() -> std::io::Result<()> {
    let args: Vec<_> = std::env::args().collect();
    let dataset = machine_learning::io::open_input_supernode(&args[1])?;
    let (train, test) = split_input(dataset);
    eprintln!("{} {}",train.len(),test.len());
    let naive = naive_sgd(&train, &test, EPOCH, BATCH).unwrap();
    let momentum = momentum_sgd(&train, &test, EPOCH, BATCH).unwrap();
    println!("Method\tType\tEpoch\tLoss\tAccuracy");
    for (epoch, result) in naive.into_iter().enumerate() {
        println!("Naive\tTrain\t{}\t{}\t{}", epoch, result.0, result.1);
        println!("Naive\tTest\t{}\t{}\t{}", epoch, result.2, result.3);
    }
    for (epoch, result) in momentum.into_iter().enumerate() {
        println!("Momentum\tTrain\t{}\t{}\t{}", epoch, result.0, result.1);
        println!("Momentum\tTest\t{}\t{}\t{}", epoch, result.2, result.3);
    }
    Ok(())
}


// Split input data set into training and validation dataset.
fn split_input(mut input:DataSet) -> (DataSet, DataSet) {
    use rand::seq::SliceRandom;
    let mut rng = thread_rng();
    input.shuffle(&mut rng);
    (input[100..].to_vec(),input[..100].to_vec())
}

// Carry out naive SGD learning on the given dataset.
fn naive_sgd(train: &DataSet, test: &DataSet, epoch: usize, batch: usize) -> Option<Vec<Summary>> {
    GNNClassifier::new_default()
        .stochastic_gradient_descent(train, epoch, batch, test)
        .map(|e| e.1)
}

// Carry out momentum SGD learning on the given dataset.
fn momentum_sgd(
    train: &DataSet,
    test: &DataSet,
    epoch: usize,
    batch: usize,
) -> Option<Vec<Summary>> {
    GNNClassifier::new_default()
        .momentum_sgd(train, epoch, batch, test)
        .map(|e| e.1)
}
