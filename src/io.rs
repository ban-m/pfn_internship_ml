use super::gnn::GraphNeuralNetwork;
use std::io::Read;
type DataSet = Vec<(GraphNeuralNetwork, bool)>;
/// Read and parse inputs.
pub fn open_input(root: &str) -> std::io::Result<DataSet> {
    // hard code
    let data_num = 2000;
    let mut data = vec![];
    for num in 0..data_num {
        let graph = format!("{}/{}_graph.txt", root, num);
        let answer = format!("{}/{}_label.txt", root, num);
        data.push((open_graph(&graph)?, open_label(&answer)?));
    }
    Ok(data)
}

pub fn open_input_supernode(root:&str)->std::io::Result<DataSet>{
    // hard code
    let data_num = 2000;
    let mut data = vec![];
    for num in 0..data_num {
        let graph = format!("{}/{}_graph.txt", root, num);
        let answer = format!("{}/{}_label.txt", root, num);
        data.push((open_graph_supernode(&graph)?, open_label(&answer)?));
    }
    Ok(data)
}

pub fn open_graph(path: &str) -> std::io::Result<GraphNeuralNetwork> {
    let mut file = std::fs::File::open(&std::path::Path::new(path))?;
    let mut input = String::new();
    file.read_to_string(&mut input)?;
    let mut nodes = 0;
    let mut graph:Vec<Vec<_>> = vec![];
    for (index,line) in input.lines().enumerate(){
        if index == 0{
            nodes = line.parse().unwrap();
        }else{
            graph.push(line.split_whitespace().map(|field| field == "1").collect())
        }
    }
    assert!(graph.len() == nodes && graph.iter().all(|e|e.len() == nodes));
    Ok(GraphNeuralNetwork::new(&graph).unwrap())
}

fn open_graph_supernode(path: &str) -> std::io::Result<GraphNeuralNetwork> {
    let mut file = std::fs::File::open(&std::path::Path::new(path))?;
    let mut input = String::new();
    file.read_to_string(&mut input)?;
    let mut nodes = 0;
    let mut graph:Vec<Vec<_>> = vec![];
    for (index,line) in input.lines().enumerate(){
        if index == 0{
            nodes = line.parse().unwrap();
        }else{
            let mut edges:Vec<_> =line.split_whitespace().map(|field| field == "1").collect();
            edges.push(true);
            graph.push(edges);
        }
    }
    graph.push(vec![true;nodes+1]);
    assert!(graph.len() == nodes +1 && graph.iter().all(|e|e.len() == nodes + 1));
    Ok(GraphNeuralNetwork::new(&graph).unwrap())
}



fn open_label(path: &str) -> std::io::Result<bool> {
    let mut file = std::fs::File::open(&std::path::Path::new(path))?;
    let mut input = String::new();
    file.read_to_string(&mut input)?;
    let label = input.lines().nth(0).unwrap() == "1";
    Ok(label)
}

