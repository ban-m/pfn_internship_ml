/// Struct to represent a graph neural wetwork.
#[derive(Debug, Clone)]
pub struct GraphNeuralNetwork {
    /// I represent edges as adjacency matrix, that is, edges[i][j] = true iff there is a edge
    /// between nodes[i] and nodes[j].
    edges: Vec<Vec<bool>>,
}

impl GraphNeuralNetwork {
    pub fn edges(&self)->&Vec<Vec<bool>>{
        &self.edges
    }
    /// Create a GNN from given parameters.
    /// If the edges contains (u,v) s.t u is not in V or v is not in V,
    /// this function returns None.
    /// This function should be the only way to construct GNN, as
    /// to assert the dimensions of each node are the same.
    pub fn new(edges: &[Vec<bool>]) -> Option<Self> {
        let edges = edges.to_vec();
        let max = edges.iter().map(Vec::len).max();
        let min = edges.iter().map(Vec::len).min();
        if max == min {
            Some(GraphNeuralNetwork { edges })
        } else {
            None
        }
    }

    /// This is a wrapper function of `self.readout` in that
    /// it uses `initial_value_default` function as a initial vectors on nodes.
    /// To more detail, see `readout`.
    pub fn readout_default(&self, weight: &[Vec<f64>], t: usize, dim: usize) -> Option<Vec<f64>> {
        let initial_value = self.initial_value_default(dim);
        self.readout(weight, t, &initial_value)
    }
    /// Calculate the readout from the given weight matrix,
    /// iteration number and initial feature vector.
    /// If the dimension of the weight is not agree with that of nodes, return None.
    /// This function also check whether or not all feature vectors on nodes have the same dimension.
    pub fn readout(
        &self,
        weight: &[Vec<f64>],
        t: usize,
        initial_feature: &[Vec<f64>],
    ) -> Option<Vec<f64>> {
        self.accept(weight, initial_feature)?;
        let dim = initial_feature[0].len();
        let mut res = vec![0.; dim];
        for feature in self.readout_inner(weight, t, initial_feature) {
            for i in 0..dim {
                res[i] += feature[i];
            }
        }
        Some(res)
    }

    // Return a initial value i.e., size dim-dimensional vectors with
    // each vector(xs) is as follows:
    // xs[0] = 1;
    // xs[i] = 0;
    fn initial_value_default(&self, dim: usize) -> Vec<Vec<f64>> {
        (0..self.edges.len())
            .map(|_| {
                let mut res = vec![0.; dim];
                res[0] = 1.;
                res
            })
            .collect()
    }

    // determine whether or not `self` accepts the input. In other words,
    // It checks intput's dimensions are consistent.
    fn accept(&self, weight: &[Vec<f64>], initial_feature: &[Vec<f64>]) -> Option<()> {
        if !initial_feature.is_empty()
            && Self::sanity_check_nodes(initial_feature)
            && self.sanity_check_edges(initial_feature.len())
        {
            let dim = initial_feature[0].len();
            if !weight.is_empty() && self.sanity_check_weight(weight, dim) {
                return Some(());
            }
        }
        None
    }

    fn sanity_check_nodes(nodes: &[Vec<f64>]) -> bool {
        let min = nodes.iter().map(Vec::len).min();
        let max = nodes.iter().map(Vec::len).max();
        min == max
    }

    fn sanity_check_edges(&self, len: usize) -> bool {
        self.edges.iter().all(|vs| vs.len() == len)
    }

    fn sanity_check_weight(&self, weight: &[Vec<f64>], dim: usize) -> bool {
        weight.len() == dim && weight.iter().all(|ws| ws.len() == dim)
    }

    // Calculate feature vector at t-step by iteratively calculating
    // gathering step 1 and 2.
    fn readout_inner(
        &self,
        weight: &[Vec<f64>],
        t: usize,
        initial_feature: &[Vec<f64>],
    ) -> Vec<Vec<f64>> {
        let dim = initial_feature[0].len();
        // To avoid unncessary memory-allocation, I use mutable array of D-dimension.
        let mut coefficients: Vec<Vec<f64>> = vec![vec![0.; dim]; initial_feature.len()];
        let mut features: Vec<Vec<f64>> = initial_feature.to_vec();
        for _ in 0..t {
            self.aggregate_first(&features, &mut coefficients);
            self.aggregate_second(&mut features, &coefficients, weight);
        }
        features
    }
    // Update all the coefficients in the GNN.
    fn aggregate_first(&self, features: &[Vec<f64>], coefs: &mut Vec<Vec<f64>>) {
        for (index, mut coef) in coefs.iter_mut().enumerate() {
            Self::aggregate_first_inner(&mut coef, features, &self.edges[index]);
        }
    }
    // Update coefficient of a feature vector of a node in the GNN.
    // The edges is a row of adjacency matrix.
    fn aggregate_first_inner(coef: &mut Vec<f64>, features: &[Vec<f64>], edges: &[bool]) {
        for (dim, elm) in coef.iter_mut().enumerate() {
            *elm = edges
                .iter()
                .enumerate()
                .filter(|(_, &b)| b)
                .map(|(i, _)| features[i][dim])
                .sum();
        }
    }
    // Update the feature vectors in the GNN.
    fn aggregate_second(
        &self,
        features: &mut Vec<Vec<f64>>,
        coefs: &[Vec<f64>],
        weight: &[Vec<f64>],
    ) {
        for (index, elm) in features.iter_mut().enumerate() {
            Self::aggregate_second_inner(elm, &coefs[index], weight);
        }
    }
    // Update a feature vector.
    fn aggregate_second_inner(feature: &mut Vec<f64>, coef: &[f64], weight: &[Vec<f64>]) {
        for (dim, elm) in feature.iter_mut().enumerate() {
            *elm = leru(inner_prod(coef, &weight[dim])) // should be positive.
        }
    }
}

fn inner_prod(xs: &[f64], ys: &[f64]) -> f64 {
    assert_eq!(xs.len(), ys.len());
    xs.iter().zip(ys.iter()).map(|(x, y)| x * y).sum::<f64>()
}

fn leru(x: f64) -> f64 {
    x.max(0.)
}

#[cfg(test)]
pub mod tests {
    use super::*;
    use rand::{distributions::Distribution, distributions::Uniform, thread_rng, Rng};
    #[test]
    fn inner_prod_test() {
        let xs = [1., 2., 3., 4.];
        let ys = [1., 2., 3., 4.];
        assert_eq!(inner_prod(&xs, &ys), 30.);
        let zeros = [0.; 100];
        let ones = [1.; 100];
        let mut rng = thread_rng();
        for _ in 0..100 {
            let xs: Vec<_> = (0..100).map(|_| rng.gen_range(-100., 100.)).collect();
            assert_eq!(inner_prod(&xs, &zeros), 0.);
            assert_eq!(inner_prod(&xs, &ones), xs.iter().sum());
        }
    }
    #[test]
    fn aggregate_second_inner() {
        let mut rng = thread_rng();
        let unif = Uniform::new(-100., 100.);
        for _ in 0..100 {
            let dim = rng.gen_range(10, 1000);
            let mut feature = vec![1.; dim];
            let coef = vec![1.; dim];
            let weight = vec![vec![1.; dim]; dim];
            GraphNeuralNetwork::aggregate_second_inner(&mut feature, &coef, &weight);
            assert_eq!(feature, vec![dim as f64; dim]);
            let coef: Vec<f64> = unif.sample_iter(&mut rng).take(dim).collect();
            let weight: Vec<Vec<f64>> = (0..dim)
                .map(|_| unif.sample_iter(&mut rng).take(dim).collect())
                .collect();
            GraphNeuralNetwork::aggregate_second_inner(&mut feature, &coef, &weight);
            assert!(feature.iter().all(|&x| x >= 0.));
        }
        let mut feature = vec![0.; 4];
        let coef = vec![1., 2., 3., 4.];
        let weight = vec![
            vec![-1., -2., -3., -4.],
            vec![1., 2., 3., 4.],
            vec![-1., -2., -3., -4.],
            vec![2., 4., 6., 8.],
        ];
        GraphNeuralNetwork::aggregate_second_inner(&mut feature, &coef, &weight);
        assert_eq!(feature, vec![0., 30., 0., 60.])
    }
    fn mock(size: usize) -> GraphNeuralNetwork {
        // return K_{size}
        GraphNeuralNetwork::new(&vec![vec![true; size]; size]).unwrap()
    }
    #[test]
    fn aggregate_second() {
        let graph = mock(4);
        let mut features = vec![vec![1., 2., 3., 4.]; 10];
        let coefs = vec![vec![1.; 4]; 10];
        let weight = vec![
            vec![-1., -2., -3., -4.],
            vec![1., 2., 3., 4.],
            vec![-1., -2., -3., -4.],
            vec![2., 4., 6., 8.],
        ];
        graph.aggregate_second(&mut features, &coefs, &weight);
        assert_eq!(features, vec![vec![0., 10., 0., 20.]; 10]);
    }
    #[test]
    fn aggregate_first_inner() {
        let edges = vec![true; 10];
        let features = vec![vec![1., 2., 3., 4.,]; 10];
        let mut coef = vec![0.; 4];
        GraphNeuralNetwork::aggregate_first_inner(&mut coef, &features, &edges);
        assert_eq!(coef, vec![10., 20., 30., 40.]);
        let edges = vec![true, true, false, true];
        let features = vec![
            vec![1., 2.],
            vec![3., 7.],
            vec![100., 10000.],
            vec![-1., -1.],
        ];
        let mut coef = vec![100.; 2];
        GraphNeuralNetwork::aggregate_first_inner(&mut coef, &features, &edges);
        assert_eq!(coef, vec![3., 8.]);
    }
    #[test]
    fn aggregate_first() {
        let mock = mock(4);
        let features = vec![vec![1.; 500]; 4];
        let mut coef = vec![vec![0.; 500]; 4];
        mock.aggregate_first(&features, &mut coef);
        assert_eq!(coef, vec![vec![4.; 500]; 4]);
        let edges = vec![
            vec![false, true, false, true],
            vec![true, false, true, false],
            vec![false, true, false, true],
            vec![true, false, true, false],
        ];
        let g = GraphNeuralNetwork::new(&edges).unwrap();
        let features = vec![vec![1., 2.], vec![2., 3.], vec![3., 4.], vec![4., 5.]];
        let mut coef = vec![vec![0.; 2]; 4];
        g.aggregate_first(&features, &mut coef);
        assert_eq!(
            coef,
            vec![vec![6., 8.], vec![4., 6.], vec![6., 8.], vec![4., 6.],]
        );
    }
    #[test]
    fn readout_inner1() {
        let edges = vec![
            vec![false, true, false, true],
            vec![true, false, true, false],
            vec![false, true, false, true],
            vec![true, false, true, false],
        ];
        let g = GraphNeuralNetwork::new(&edges).unwrap();
        let features = vec![vec![1., 2.], vec![2., 3.], vec![3., 4.], vec![4., 5.]];
        let weight = vec![vec![-1., 2.], vec![2., -1.]];
        let result = g.readout(&weight, 1, &features).unwrap();
        assert_eq!(result, vec![36., 12.])
    }

    #[test]
    fn readout_inner2() {
        // If each element of W is less than 1/(# of nodes) / D,
        // each value of on each node v monotonously decrease,
        // that is, after very long time, the values would deminish.
        let mut rng = thread_rng();
        let d = 20;
        let nodes = 10;
        let p = 0.01;
        let t = 5;
        let weight_gen = Uniform::new(0., 1. / (d * nodes) as f64);
        let init_gen = Uniform::new(-1., 100.);
        for _ in 0..t {
            let weight: Vec<Vec<_>> = (0..d)
                .map(|_| weight_gen.sample_iter(&mut rng).take(d).collect())
                .collect();
            let edges: Vec<Vec<_>> = (0..nodes)
                .map(|_| (0..nodes).map(|_| rng.gen_bool(p)).collect())
                .collect();
            let g = GraphNeuralNetwork::new(&edges).unwrap();
            let initial_feature: Vec<Vec<_>> = (0..nodes)
                .map(|_| init_gen.sample_iter(&mut rng).take(d).collect())
                .collect();
            let result = g.readout(&weight, 10000, &initial_feature).unwrap();
            assert!(result.into_iter().all(|e| e < 0.01));
        }
    }
    #[test]
    fn readout_test3() {
        let d = 2;
        let nodes = 10;
        // Identity matrix.
        let weight: Vec<Vec<_>> = (0..d)
            .map(|i| (0..d).map(|j| if i == j { 1. } else { 0. }).collect())
            .collect();
        let edges: Vec<Vec<_>> = (0..nodes)
            .map(|i| (0..nodes).map(|j| j == ((i + 1) % nodes)).collect())
            .collect();
        let mut rng = thread_rng();
        let g = GraphNeuralNetwork::new(&edges).unwrap();
        let init_gen = Uniform::new(1., 10.);
        for _ in 0..100 {
            let initial_feature: Vec<Vec<_>> = (0..nodes)
                .map(|_| init_gen.sample_iter(&mut rng).take(d).collect())
                .collect();
            let result = g.readout_inner(&weight, 1, &initial_feature);
            let answer: Vec<Vec<_>> = initial_feature[1..]
                .iter()
                .chain(initial_feature[0..1].iter())
                .map(|e| e.clone())
                .collect();
            for (r,a) in result.into_iter().zip(answer.into_iter()){
                for i in 0..d{
                    assert!((r[i] - a[i]).abs() < 0.001);
                }
            }
            let result = g.readout(&weight, 1, &initial_feature).unwrap();
            let mut answer = vec![0.; d];
            for v in initial_feature {
                for i in 0..d {
                    answer[i] += v[i];
                }
            }
            for i in 0..d{
                assert!((result[i]-answer[i]).abs() < 0.001);
            }
        }
    }
}
