//! A tiny library of graph neural network(GNN).
extern crate rand;
use rand::distributions::Normal;
use rand::{distributions::Distribution, distributions::Uniform, thread_rng, Rng};
pub mod gnn;
pub mod io;
type DataSet<'a> = &'a [(gnn::GraphNeuralNetwork, bool)];
// The loss and accuracy of trainning dataset and the those of test dataset.
type Summary = (f64, f64, f64, f64);
/// A struct to classify given graph. It is this struct that
/// have all parameters used to predict a graph.
/// Notably, one should supply a graph neural network to
/// this struct when *predicting*, not when *constructing*.
/// In short, it predict a given graph from the readout(rs) as follows:
/// 1. t1 <- rs * folder // inner-product.
/// 2. t2 <- t1 + offset // offset padding.
/// 3. p <- 1/(1+exp(-t2)) // Sigmoid.
/// 4. p > 1/2 ? true : false
#[derive(Debug, Clone)]
pub struct GNNClassifier {
    parameters: GNNParameter,
}

/// Parameter set which a GNN classifler has. It is also used as the gradient of the classifier.
/// It has three members: weight matrix(W), folder vector(A), and offset scalar(b).
/// Remark: I implement the binary operator + and - such that they have type: Self X Self -> Self, whereas the binary operator * is implemented with its type: Self X f64 -> Self.
/// In other words, multiplication is vector-scalar multiplication.
/// It is the same as division operator(/).
#[derive(Debug, Clone)]
pub struct GNNParameter {
    /// the dimention of the values. This value is used to
    /// assert that the weight and the folder has
    /// dim X dim and dim dimension respectively.
    dim: usize,
    /// Weight given to a GNN.
    weight: Vec<Vec<f64>>,
    /// Folder used to generate consensus value,
    /// In other words, I take the inner-product
    /// between the d dimentional vector
    /// generated from given GNN and
    /// this vector to get 1-dimensional real value.
    folder: Vec<f64>,
    /// Offset is used to *lift up* the inner product
    /// of readout and folder.
    offset: f64,
}

impl GNNParameter {
    /// Create a gradient from given parameters.
    pub fn from(dim: usize, weight: Vec<Vec<f64>>, folder: Vec<f64>, offset: f64) -> Self {
        GNNParameter {
            dim,
            weight,
            folder,
            offset,
        }
    }
    /// Create 0-vectors with given dimension.
    pub fn new(dim: usize) -> Self {
        let weight = vec![vec![0.; dim]; dim];
        let folder = vec![0.; dim];
        let offset = 0.;
        Self::from(dim, weight, folder, offset)
    }
}

impl std::ops::Add<&GNNParameter> for GNNParameter {
    type Output = GNNParameter;
    fn add(mut self, g2: &GNNParameter) -> GNNParameter {
        assert_eq!(self.dim, g2.dim);
        for i in 0..self.dim {
            for j in 0..self.dim {
                self.weight[i][j] += g2.weight[i][j];
            }
            self.folder[i] += g2.folder[i];
        }
        self.offset += g2.offset;
        self
    }
}

impl std::ops::Sub<&GNNParameter> for GNNParameter {
    type Output = GNNParameter;
    fn sub(mut self, g2: &GNNParameter) -> GNNParameter {
        assert_eq!(self.dim, g2.dim);
        for i in 0..self.dim {
            for j in 0..self.dim {
                self.weight[i][j] -= g2.weight[i][j];
            }
            self.folder[i] -= g2.folder[i];
        }
        self.offset -= g2.offset;
        self
    }
}

impl std::ops::Mul<f64> for GNNParameter {
    type Output = GNNParameter;
    fn mul(mut self, mul: f64) -> GNNParameter {
        for i in 0..self.dim {
            for j in 0..self.dim {
                self.weight[i][j] *= mul;
            }
            self.folder[i] *= mul;
        }
        self.offset *= mul;
        self
    }
}

impl std::ops::Div<f64> for GNNParameter {
    type Output = GNNParameter;
    fn div(mut self, div: f64) -> GNNParameter {
        for i in 0..self.dim {
            for j in 0..self.dim {
                self.weight[i][j] /= div;
            }
            self.folder[i] /= div;
        }
        self.offset /= div;
        self
    }
}

// The number of iteration.
const T: usize = 2;
// The difference used to calculate a gradient.
const EP: f64 = 0.001;
// The learning rate
const ALPHA: f64 = 0.0001;
// The moment
const ETA: f64 = 0.8;
// The dimension of feature vector.
const DIM:usize = 8;

impl GNNClassifier {
    fn weight(&self) -> &Vec<Vec<f64>> {
        &self.parameters.weight
    }
    fn folder(&self) -> &Vec<f64> {
        &self.parameters.folder
    }
    fn offset(&self) -> f64 {
        self.parameters.offset
    }
    fn dim(&self) -> usize {
        self.parameters.dim
    }
    /// Generate a new GNN classifier from the given parameters.
    pub fn new(weight: &[Vec<f64>], folder: &[f64], offset: f64) -> Option<Self> {
        if !weight.is_empty() && !folder.is_empty() {
            let dim = folder.len();
            if weight.len() == dim && weight.iter().all(|xs| xs.len() == dim) {
                return Some(Self {
                    parameters: GNNParameter::from(dim, weight.to_vec(), folder.to_vec(), offset),
                });
            }
        }
        None
    }
    /// Generate a default GNN classifier.
    pub fn new_default() -> Self {
        let mut rng = thread_rng();
        let norm = Normal::new(0., 0.4);
        let dim = DIM;
        let weight: Vec<_> = (0..dim)
            .map(|_| norm.sample_iter(&mut rng).take(dim).collect())
            .collect();
        let folder: Vec<_> = norm.sample_iter(&mut rng).take(dim).collect();
        let offset = 0.;
        Self::new(&weight, &folder, offset).unwrap() // always does not panic.
    }
    /// Calculate the summary of given training dat and test dataset.
    /// In other words, this function compute the *average* loss and overall accuaracy.
    pub fn get_summary<'a>(&self, train: DataSet<'a>, test: DataSet<'a>) -> Summary {
        let (train_loss, train_acc) = self.get_summary_inner(train);
        let (test_loss, test_acc) = self.get_summary_inner(test);
        (train_loss, train_acc, test_loss, test_acc)
    }
    fn get_summary_inner<'a>(&self, data: DataSet<'a>) -> (f64, f64) {
        let len = data.len() as f64;
        let loss: f64 = data.iter().filter_map(|(gnn, y)| self.loss(gnn, *y)).sum();
        let acc: f64 = data
            .iter()
            .filter_map(|(gnn, y)| self.predict(gnn).map(|p| p == *y))
            .map(|e| if e { 1. } else { 0. })
            .sum();
        (loss / len, acc / len)
    }
    /// The predict function.
    pub fn predict(&self, gnn: &gnn::GraphNeuralNetwork) -> Option<bool> {
        let readout = gnn.readout_default(&self.weight(), T, self.dim())?;
        let inner_prod: f64 = readout
            .iter()
            .zip(self.folder().iter())
            .map(|(&a, &b)| a * b)
            .sum();
        let padding = inner_prod + self.offset();
        let sigmoid = 1. / (1. + (-1. * padding).exp());
        Some(sigmoid > 0.5)
    }
    /// The loss function.
    /// Noteworthy, the loss function
    /// -y*ln(p) - (1-y)*ln(1-p)
    /// is equivalent to
    /// -ys + ln(1+exp(s))
    /// where p = 1/(1+exp(-s)).
    /// Thus, whenever s is sufficiently large,
    /// I approximate 1 + exp(s) as exp(s), which means loss is -ys + s.
    pub fn loss(&self, gnn: &gnn::GraphNeuralNetwork, y: bool) -> Option<f64> {
        let readout = gnn.readout_default(&self.weight(), T, self.dim())?;
        let inner_prod: f64 = readout
            .iter()
            .zip(self.folder().iter())
            .map(|(&a, &b)| a * b)
            .sum();
        let padding = inner_prod + self.offset();
        let y = if y { 1.0 } else { 0. };
        if padding > 30. {
            Some(-y * padding + padding)
        } else {
            Some(-y * padding + (1. + padding.exp()).ln())
        }
    }
    /// Update parameters by numerically calculate gradients for each parameters.
    /// Return Some(self) if the update succesed, return None otherwise.
    pub fn naive_update(mut self, gnn: &gnn::GraphNeuralNetwork, y: bool) -> Option<Self> {
        let gradient = self.gradient(gnn, y)? * ALPHA;
        self.parameters = self.parameters - &gradient;
        Some(self)
    }
    // Compute the gradient of each element for a given graph neural network.
    fn gradient(&mut self, gnn: &gnn::GraphNeuralNetwork, y: bool) -> Option<GNNParameter> {
        // Note that, I modify the self.edges on-the-fly iteratively.
        // this is O.K, when I use exactly the same epsilon.
        let loss = self.loss(gnn, y)?;
        let mut weight_gradient = vec![vec![0.; self.dim()]; self.dim()];
        let mut folder_gradient = vec![0.; self.dim()];
        // Only update once.
        let offset_gradient;
        for i in 0..self.dim() {
            for j in 0..self.dim() {
                // update weight
                self.parameters.weight[i][j] += EP;
                weight_gradient[i][j] = (self.loss(gnn, y)? - loss) / EP;
                self.parameters.weight[i][j] -= EP;
            }
            // update folder
            self.parameters.folder[i] += EP;
            folder_gradient[i] = (self.loss(gnn, y)? - loss) / EP;
            self.parameters.folder[i] -= EP;
        }
        // update offset
        self.parameters.offset += EP;
        offset_gradient = (self.loss(gnn, y)? - loss) / EP;
        self.parameters.offset -= EP;
        Some(GNNParameter::from(
            self.dim(),
            weight_gradient,
            folder_gradient,
            offset_gradient,
        ))
    }
    /// Implementation of vanilla SGD.
    /// Return None if update failed at any iteartion.
    /// Return Some((self,Summary)) if all itetaion successfully evaluated.
    /// User need to supply not only training data but also test(validation) dataset
    /// so that one can trace its "growth."
    pub fn stochastic_gradient_descent<'a>(
        mut self,
        train: DataSet<'a>,
        epoch: usize,
        batch: usize,
        test: DataSet<'a>,
    ) -> Option<(Self, Vec<Summary>)> {
        let mut res = vec![];
        for _ in 0..epoch {
            self = self.sgd_inner(train, batch)?;
            res.push(self.get_summary(train, test));
        }
        Some((self, res))
    }
    // Implementation of an epoch in a naive SGD.
    fn sgd_inner(mut self, data: DataSet, batch: usize) -> Option<Self> {
        let shuffled = Self::shuffle(data);
        for chunk in shuffled.chunks(batch) {
            let update = self.sgd_grad_batch(chunk)? * ALPHA;
            self.parameters = self.parameters - &update;
        }
        Some(self)
    }
    /// Momentum Stochastic gradient descent.
    /// Return None if update failed at any iteartion.
    /// Return Some((self,Summary)) if all itetaion successfully evaluated.
    /// User need to supply not only training data but also test(validation) dataset
    /// so that one can trace its "growth."
    /// Note that the hyper parameter such as
    /// Moment is hard coded at the beggining of this module.
    /// Thus, if one wish to tuning the parameter, please modify at that location
    /// or inject dependencies at this function.
    pub fn momentum_sgd<'a>(
        mut self,
        train: DataSet<'a>,
        epoch: usize,
        batch: usize,
        test: DataSet<'a>,
    ) -> Option<(Self, Vec<Summary>)> {
        let mut res = vec![];
        for _ in 0..epoch {
            let shuffled = Self::shuffle(train);
            let mut moment = GNNParameter::new(self.dim());
            for chunk in shuffled.chunks(batch) {
                moment = (moment * ETA) - &(self.sgd_grad_batch(chunk)? * ALPHA);
                self.parameters = self.parameters + &moment;
            }
            res.push(self.get_summary(train, test));
        }
        Some((self, res))
    }
    // shuffle input slice
    fn shuffle<'a>(data: DataSet<'a>) -> Vec<&'a (gnn::GraphNeuralNetwork, bool)> {
        let mut slice_to_data: Vec<&(gnn::GraphNeuralNetwork, bool)> = data.iter().collect();
        let mut rng = thread_rng();
        use rand::seq::SliceRandom;
        slice_to_data.shuffle(&mut rng);
        slice_to_data
    }
    // Calculate the average gradient for the given batch.
    fn sgd_grad_batch(
        &mut self,
        batch: &[&(gnn::GraphNeuralNetwork, bool)],
    ) -> Option<GNNParameter> {
        let mut overall_grad = GNNParameter::new(self.dim());
        for (ref gnn, y) in batch {
            overall_grad = overall_grad + &self.gradient(gnn, *y)?;
        }
        Some(overall_grad/ batch.len() as f64)
    }
}
